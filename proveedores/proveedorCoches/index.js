'use strict'

const config = require('../../config');

// DB
const URL_DB = config.URL_DB;
const mongojs = require('mongojs');
var db = mongojs(URL_DB);

const port = process.env.PORT || config.portDictionary["proveedorCoches"];
const URL_PROVEEDOR = config.urlDictionary["proveedorCoches"];
const ID_PROVEEDOR = "1111";
const MAX_ID_CODE = config.MAX_ID_CODE;
const MIN_ID_CODE = config.MIN_ID_CODE;

const express = require('express');
const logger = require('morgan');
const https = require('https');
const fs = require('fs');
const helmet = require('helmet');
const moment = require('moment');

const app = express();
const HTTPS_OPTIONS = {
    key: fs.readFileSync('../../certificates/key.pem'),
    cert: fs.readFileSync('../../certificates/cert.pem')
}
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";


// Declaramos los middleware
app.use(logger('dev'));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(helmet());

function filterObj(source, whiteList) {
    const res = {};
    // iterate over each keys of source
    Object.keys(source).forEach((key) => {
        // if whiteList contains the current key, add this key to res
        if (whiteList.indexOf(key) !== -1) {
            res[key] = source[key];
        }
    });
    return res;
}

function idCode() {
    return Math.floor(Math.random() * (MAX_ID_CODE - MIN_ID_CODE) + MIN_ID_CODE);
}

function price(minPrice, maxPrice) {
    const price = (Math.random() * (maxPrice - minPrice) + Number(minPrice));
    return price - (price % 0.01);
}


// Iniciamos la escucha
https.createServer(HTTPS_OPTIONS, app)
    .listen(port, () => {
        console.log(`PROVEEDOR de COCHES ejecutándose en ${URL_PROVEEDOR}`);
    });

// Declaramos nuestras rutas y nuestros controladores

// Ofrecer servicios
app.get('/api/coches/', (req, res, next) => {

    // Array que sirve para filtrar los parámetros contemplados por
    // nuestra agencia 
    const filterArray = [
        "ciudad",
        "fechaInicio",
        "fechaFin",
        "numPlazas",
        "marca",
        "modelo"
    ];

    const queries = req.query;
    console.log(queries);

    let filteredQueries = filterObj(queries, filterArray);

    // Calculo el precio
    if (queries.precioMin && queries.precioMax) {
        filteredQueries.precio = price(queries.precioMin, queries.precioMax);
    } else if (queries.fechaInicio && queries.fechaFin) {
        const diasReserva = moment(queries.fechaFin).diff(queries.fechaInicio) / 86400000;
        filteredQueries.precio = 97.13 * diasReserva;
    } else {
        filteredQueries.precio = 200;
    }

    filteredQueries.id = idCode().toString();
    filteredQueries.reservado = "no";
    console.log(filteredQueries);

    try {
        db.proveedorCoches.save(filteredQueries, (err, elementoGuardado) => {
            if (err || !elementoGuardado) { throw err; }
            else {
                console.log(`Se ha realizado la oferta: ${elementoGuardado}`);
                res.status(200).json({
                    result: 'OK',
                    object: elementoGuardado
                });
            }
        });
    } catch (err) {
        console.log(`ERROR al reservar el coche ${id}`);
        res.status(500).json({
            result: 'KO'
        });
    }
});

// RESERVAR COCHES
app.post('/api/coches', (req, res, next) => {
    const id = req.body.id;
    try {
        db.proveedorCoches.findOne({ id: id }, (err, resultado) => {
            if (err || !resultado) {
                console.log(`ERROR al obtener DB.proveedorCoches.${id}`);
                res.status(404).json({
                    result: 'KO',
                    message: `ERROR No existe la oferta con id: ${id}`
                });
                return next(err);
            } else if (resultado.reservado == "si") {
                console.log(`ERROR al reservar DB.proveedorCoches.${id}, YA RESERVADO`);
                res.status(410).json({
                    result: 'KO',
                    message: `La oferta con id ${id} ya no está disponible`
                });
            } else {
                db.proveedorCoches.update({ id: id },
                    { $set: { reservado: "si" } },
                    { safe: true, multi: false },
                    (err, resultadoUpdate) => {
                        if (err || !resultadoUpdate) {
                            console.log(`ERROR al marcar como reservado DB.proveedorCoches.${id}`);
                            res.status(404).json({
                                result: 'KO',
                                message: `ERROR No se ha podido guardar la reserva: ${id}`
                            });
                            return next(err);
                        } else {
                            resultado.reservado = "si";
                            res.status(201).json({
                                result: 'OK',
                                id: id,
                                idProveedor: ID_PROVEEDOR,
                                caracteristicas: resultado
                            });
                        }
                    });
            }
        });
    } catch (err) {
        console.log(`ERROR al reservar el coche ${id}`);
        res.status(500).json({
            result: 'KO'
        });
    }
});


// ELIMINAR RESERVAS DE COCHES
app.delete('/api/coches/:id', (req, res, next) => {
    const id = req.params.id;
    try {
        db.proveedorCoches.update({ id: id },
            { $set: { reservado: "no" } },
            { safe: true, multi: false },
            (err, resultadoUpdate) => {
                if (err || !resultadoUpdate) {
                    console.log(`ERROR al marcar como NO reservado DB.proveedorCoches.${id}`);
                    // res.status(404).json({
                    //     result: 'KO',
                    //     message: `ERROR No se ha podido guardar la cancelación de reserva: ${id}`
                    // });
                    return next(err);
                } else {
                    res.status(200).json({
                        result: 'OK',
                        id: id,
                        idProveedor: ID_PROVEEDOR
                    });
                }
            });
    } catch (err) {
        console.log(`ERROR al cancelar la reserva de coche ${id}`);
        res.status(500).json({
            result: 'KO'
        });
    }
});

