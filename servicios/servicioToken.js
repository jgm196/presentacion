'use strict'

const config = require('../config');
const jwt = require('jwt-simple');
const moment = require('moment');

const SECRET = config.SECRET;
const EXP_TIME = config.TOKEN_EXP_TIME;

/**
 * 
 *      CREAR TOKEN
 * 
 */

function generarToken(usuario) {
    const payload = {
        sub: usuario.userName,
        iat: moment().unix(),
        exp: moment().add(EXP_TIME, 'minutes').unix()
    }
    return jwt.encode(payload, SECRET);
}


/**
 * 
 *     VERIFICAR TOKEN
 * 
 */

function verificarToken(token) {
    return new Promise((resolve, reject) => {
        try {
            const payload = jwt.decode(token, SECRET, true);
            if (payload.exp <= moment().unix()) {
                reject({
                    status: 401,
                    message: 'El token ha caducado'
                });
            }
            resolve(payload.sub);
        } catch (err) {
            reject({
                status: 500,
                message: 'El token no es válido'
            });
        }
    });

}

module.exports = {
    generarToken,
    verificarToken
}