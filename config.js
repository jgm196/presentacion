const portDictionary = {
    gateway: 4100,
    usuarios: 4000,
    coches: 4001,
    proveedorCoches: 4010,
    vuelos: 4002,
    proveedorVuelos: 4020,
    hoteles: 4003,
    proveedorHoteles: 4030,
    database: 4004,
    pagos: 4005,
    proveedorBancos: 4050,
    paquetes: 4006,
    transacciones: 4007
}

const urlDictionary = {
    gateway: `https://localhost:${portDictionary["gateway"]}/api`,
    usuarios: `https://localhost:${portDictionary["usuarios"]}/api/usuarios`,
    coches : `https://localhost:${portDictionary["coches"]}/api/coches`,
    proveedorCoches: `https://localhost:${portDictionary["proveedorCoches"]}/api/coches`,
    vuelos : `https://localhost:${portDictionary["vuelos"]}/api/vuelos`,
    proveedorVuelos: `https://localhost:${portDictionary["proveedorVuelos"]}/api/vuelos`,
    hoteles : `https://localhost:${portDictionary["hoteles"]}/api/hoteles`,
    proveedorHoteles: `https://localhost:${portDictionary["proveedorHoteles"]}/api/hoteles`,
    database: `https://localhost:${portDictionary["database"]}/api/database`,
    pagos: `https://localhost:${portDictionary["pagos"]}/api/pagos`,
    proveedorBancos: `https://localhost:${portDictionary["proveedorBancos"]}/api/pagos`,
    paquetes: `https://localhost:${portDictionary["paquetes"]}/api/paquetes`,
    transacciones: `https://localhost:${portDictionary["transacciones"]}/api/transacciones`
}

module.exports = {
    URL_DB: 'mongodb+srv://javi:contra@sd.h6ohw.mongodb.net/SD?retryWrites=true&w=majority',
    MAX_ID_CODE: 999999,
    MIN_ID_CODE: 100000,
    CUENTA_PROVEEDOR: "ES123456789PROV",
    SECRET: 'askhdbfdsbfaksdbfsbdg',
    // SECRET: 'AAAwEAAQAAAYEA4brWSPQW416Fc6xtR5aFQivkup6FC4JUUhwK+NxVdTotEJTve7R',
    TOKEN_EXP_TIME: 7*24*60, // Una semana en minutos
    SALT_ROUNDS: 10,
    REQUIRE_AUTH: true,
    urlDictionary,
    portDictionary
}