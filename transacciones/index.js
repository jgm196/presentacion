'use strict'

const config = require('../config');

const port = process.env.PORT || config.portDictionary["transacciones"];
const URL_WS = config.urlDictionary["transacciones"];
const URL_MOD_DB = config.urlDictionary["database"] + "/transacciones";

const express = require('express');
const logger = require('morgan');
const fetch = require('node-fetch');
const mongojs = require('mongojs');
const fs = require('fs');
const https = require('https');
const helmet = require('helmet');

const app = express();
const HTTPS_OPTIONS = {
    key: fs.readFileSync('../certificates/key.pem'),
    cert: fs.readFileSync('../certificates/cert.pem')
}
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";


const URL_DB = config.URL_DB;
var db = mongojs(URL_DB);

// Declaramos los middleware
app.use(logger('dev'));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(helmet());

// Iniciamos la escucha
https.createServer(HTTPS_OPTIONS, app)
    .listen(port, () => {
        console.log(`GESTOR DE TRANSACCIONES ejecutándose en ${URL_WS}`);
    });

// Crear transacción
app.post('/api/transacciones', (req, res, next) => {
    const idPaquete = req.body.idPaquete;
    console.log(`Iniciando transacción de reserva del paquete: ${idPaquete}`);
    fetch(`${URL_MOD_DB}`, {
        method: 'POST',
        body: JSON.stringify(req.body),
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(res => res.json())
        .then(json => {
            console.log(`Transacción del paquete ${idPaquete} iniciada con éxito`);
            res.status(201).json(json);
        })
        .catch(err => {
            console.log(err);
            return res.status(401).json({
                result: 'KO',
                mensaje: err
            });
        });
});

// Modificar estado de la transacción
app.put('/api/transacciones/:idPaquete', (req, res, next) => {
    try {
        const idPaquete = req.params.idPaquete;

        if (req.body.estadoTransaccion == 'Finalizada') {
            const query = {};
            query["idPaquete"] = idPaquete;
            db.transacciones.remove(
                query,
                (err, resultado) => {
                    if (err) return next(err);

                    console.log(`Se ha completado la transacción ${idPaquete}`);
                    res.status(200).json({
                        result: 'OK',
                        object: resultado
                    });
                });
        } else {

            var searchQuery = {};
            searchQuery["idPaquete"] = idPaquete;

            var addQuery = {};
            addQuery[req.body.tipoServicio] = req.body.elemento;

            db.transacciones.update(
                searchQuery,
                { $addToSet: addQuery },
                (err, resultado) => {
                    if (err) return next(err);

                    console.log(`MODIFICADO estado de: elemento ${req.body.elemento.id} || transacción ${idPaquete}`);
                    res.status(200).json({
                        result: 'OK',
                        object: resultado
                    });
                });
        }
    } catch (err) {
        console.log(`ERROR al modificar: elemento ${req.body.elemento.id} || transacción ${idPaquete}`);
        res.status(500).json({
            result: 'KO'
        });
    }
});

// Rollback
app.delete('/api/transacciones/:idPaquete', (req, res, next) => {
    const idPaquete = req.params.idPaquete;

    try {

        const query = {};
        query["idPaquete"] = idPaquete;
        db.transacciones.findOne(query, (err, transaccion) => {
            if (err || !transaccion) {
                console.log(`ERROR al obtener la transacción ${idPaquete}`);
                return next(err);
            }

            for (const servicios in transaccion) {
                if (transaccion[servicios] instanceof Array) {
                    transaccion[servicios].forEach(servicio => {

                        fetch(`${config.urlDictionary[servicios]}/${servicio.id}`, {
                            method: 'DELETE',
                            headers: {
                                'Content-Type': 'application/json'
                            }
                        })
                            .then(console.log(`CANCELADO ${servicios} ${servicio.id} (Paquete: ${idPaquete})`))
                            .catch(err => {
                                console.log(err);
                                return res.status(401).json({
                                    result: 'KO',
                                    mensaje: err
                                });
                            });
                    });
                }
            }
        }).then(
            res.status(201).json({
                result: 'OK',
                idPaquete: idPaquete
            }));

    } catch (err) {
        console.log(`ERROR al hacer rollback en la transacción ${idPaquete}`);
        res.status(500).json({
            result: 'KO'
        });
    }
});





